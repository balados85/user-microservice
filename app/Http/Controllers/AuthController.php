<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Str;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Password;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Auth;


class AuthController extends Controller
{
    use SendsPasswordResetEmails;

    public function forgot(Request $request)
    {
        $message = [
            'exists:users' => 'You have entered an invalid email address',
            'required' => 'Email field cannot be empty',
            'email' => 'Please make sure you entered a valid email address'
        ];

        $validator = Validator::make($request->all(), [
            'email' => [
                'required',
                'email',
                'exists:users'
            ]
        ], $message);

        /**
         * Check if all validation conditions have passed
         */
        if ($validator->fails()) {
            return response()->json([$validator->errors()], 400);
        }

        $credentials = ['email' => $request->input('email')];
        $response = Password::sendResetLink($credentials, function (Message $message) {
            $message->subject($this->getEmailSubject());
        });

        $success['message'] = 'Reset instructions sent successfully! Check your email';

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return response()->json($success, 200);
            case Password::INVALID_USER:
                return response()->json(['error' => 'invalid_user', 'message' => 'You have entered an invalid email address'], 400);
        }
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function reset(Request $request) {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:8',
        ]);

        /**
         * Check if all validation conditions have passed
         */
        if ($validator->fails()) {
            return response()->json([$validator->errors()], 400);
        }

        $password = $request->password;
        $tokenData = DB::table('password_resets')
            ->where('token', $request->input('token'))->first();
        
        if(!$tokenData) {
            return response()->json(['error'=>'token_error', 'message'=>'Token is invalid.'], 400);
        }
            
        $user = User::where('email', $tokenData->email)->first();
        
        if (!$user) {
            return response()->json(['error'=>'user not found', 'message'=>'No user exists'], 400);
        }

        $user->password = Hash::make($password);

        $user->setRememberToken(Str::random(60));

        $user->save();

        event(new PasswordReset($user));

        // If the user shouldn't reuse the token later, delete the token 
        DB::table('password_resets')->where('email', $user->email)->delete();

        return response()->json(['success'=>'password reset', 'message'=>'User password reset successful'], 400);

    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

     /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([$validator->errors()], 400);
        }

        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            if($user->active_status){

                $new_session_id = \Session::getId(); //get new session_id after user sign in
                $last_session = \Session::getHandler()->read($user->last_session_id); // retrive last session

                if ($last_session) {
                   return response()->json(
                        [
                            'error' => 'authentication_failed',
                            'message' => 'You are already logged in!'
                        ],
                        401
                    ); 
                }

                $user->last_session_id = $new_session_id;
                $user->save();

                $token = Auth::user()->createToken('talamus-ui')->accessToken;
                if(!$token){
                    return response()->json(
                        [
                            'error' => 'authentication_failed',
                            'message' => 'Token not created'
                        ],
                        402
                    ); 
                }
                $success['access_token'] =  $token;
                //$success['access_token'] = "12ddg4th7kzxs5yrea";

                return response()->json($success, 200); 
            }
            return response()->json(
                [
                    'error' => 'authentication_failed',
                    'message' => 'You have been deactivated. Contact admin!'
                ],
                401
            );
            
        }
        return response()->json(
            [
                'error' => 'authentication_failed',
                'message' => 'Email Address and password are incorrect. Please Try Again!'
            ],
            401
        );
    }

    /*public function logout($id){
        //user = Auth::user()->token();
        //$user->revoke();

        //$this->removeSession($request->input["email"]);
        
        return response()->json(["message" => "User successfully logged out"], 200);
    }

    public function removeSession($email){
        $user = DB::table('users')->where('email',$email)->first();
        \Session::getHandler()->destroy($user->last_session_id);
        DB::table('users')->where('email', $email)->update(['last_session_id' => NULL]);
        //$user->last_session_id = NULL;
         //$user->save();
    }*/
}    