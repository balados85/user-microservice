<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
// use App\Role;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Auth;


class RolesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return response()->json(Role::with('permissions')->
        where('company_id', $user->company_id)->get(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Role::where('name',$request->name)->count() > 0) {
            return response()->json(["message" => "Role exists already, please enter a different name"], 400);
        }

        $user = Auth::user();

        $role = Role::create(['guard_name' => 'api', 
        'name' => $request->post("name"), 'company_id' => $user->company_id,
        'role_type' => $request->post("role_type")]);

        $permissions = $request->post("permissions");
        foreach($permissions as $permission){
            $permission = Permission::where('name', $permission)->get();
            $role->givePermissionTo($permission);
        }

        return response()->json($role, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        return response()->json(Role::with('permissions')->find($id), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        if($role->name != $request->name) {
            // Check if role name exists already
            if(Role::where('name',$request->name)->count() > 0) {
                return response()->json(["message" => "Role exists already, please enter a different name"], 400);
            }
        }
        
        $role->update(['guard_name' => 'api', 'name' => $request->post("name"), 'role_type' => $request->post("role_type")]);

        $permissions = $request->post("permissions");
        foreach($permissions as $permission){
            $permission = Permission::where('name', $permission)->get();
            $role->revokePermissionTo($permission);
            $role->givePermissionTo($permission);
        }
        return response()->json($role, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        return response()->json($role->delete(), 204);
    }

    public function rolebytype($type){
        $role = Role::where('role_type', $type)->get();

        return response()->json($role, 200);
    }
}
