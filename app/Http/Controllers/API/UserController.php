<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Notifications\UserNotification;
use Illuminate\Support\Facades\DB;


class UserController extends Controller
{
    public $successStatus = 200;
    
    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */
    public function store(Request $request)
    {
        /**
         * Validate user registration fields
         */
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
            'c_password' => 'required|same:password',
            'role_id'=> 'required'
        ], ['role_id.required' => 'The role field is required']);

        /**
         * Check if all validation conditions have passed
         */
        if ($validator->fails()) {
            return response()->json([$validator->errors()], 400);
        }

        $input = $request->all();

        // Encrypt password
        $input['password'] = bcrypt($input['password']);

        // Handles user creation process
        $user = User::create($input);
        $permissions = Permission::where('id',$user->role_id)->get();
        $user->role->permissions = $permissions;

        //$user->email = "imcsey@claimsync.com";
        //$user->email = $input['email'];
        $message = ["name"=>$input['name'], 
                    "email"=> $input['email'],
                    "password"=>$input['password']
                ];
        
        $user->notify(new UserNotification($message));

        return response()->json($user, 201);
    }

    /**
     * Get all users
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $response = $user->user_type == 'TAL' ? User::with('role')->get() : User::with('role')->where('company_id', $user->company_id)->get();

        return response()->json($response, $this->successStatus);
    }

    /**
     * Get user
     * 
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $user = User::with('role')->find($id);
        $permissions = Permission::where('id',$user->role_id)->get();
        $user->role->permissions = $permissions;
        return response()->json($user, $this->successStatus);
    }

    /**
     * Update user
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        // Check if email is different from email supplied by update
        if($request->input('email') !== $user->email){
            // Check if email exists already
            if ( User::where('email', $request->input('email'))->count() > 0 ) {
                return response()->json(['error'=> 'Email address exists. Please use different email address']);
            }
        }

        $user->update($request->all());
        return response()->json($user, $this->successStatus);
    }

    public function delete(Request $request, User $user)
    {
        $user->delete();
        return response()->json(null, 204);
    }

    /**
     * Get count of users
     * 
     * @return \Illuminate\Http\Response
     */
    public function count()
    {
        $user = Auth::user();
        $response = $user->user_type == 'TAL' ? User::with('role')->get() : User::with('role')->where('company_id', $user->company_id)->get();
        $count = array('count' => $response->count());
        return response()->json($count, $this->successStatus);
    }

    public function toggle($id,$status){
        $user = User::find($id);
        $user->active_status = $status;
        $user->save();
        return response()->json(['success'=>'User disabled']);
    }

    public function logout($id){
        $user = Auth::user()->token();
        $user->revoke();
        $user = DB::table('users')->find($id);
        \Session::getHandler()->destroy($user->last_session_id);
        DB::table('users')->where("id", $id)->update(['last_session_id' => NULL]);
        
        return response()->json(["message" => "Logout successfully"], 200);
    }

}
