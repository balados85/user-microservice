<?php

namespace App;

class Role extends \Spatie\Permission\Models\Role
{
    protected $fillable = ['name', 'company_id'];

    /**
     * Get the role associated with the user.
     */
    public function permissions()
    {
        return $this->belongsTo('\Spatie\Permission\Models\Permission');
    }
}
