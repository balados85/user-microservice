<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'AuthController@login');
Route::post('auth/forgot', 'AuthController@forgot');
Route::post('auth/reset', 'AuthController@reset');
//Route::put('logout/{id}', 'AuthController@logout');

Route::middleware('auth:api')->group(function () {
    Route::post('users', 'API\UserController@store');
    Route::get('users', 'API\UserController@index');
    Route::get('users/{id}/{status_code}', 'API\UserController@show');
    Route::get('count', 'API\UserController@count');
    Route::put('users/{user}', 'API\UserController@update');
    Route::patch('users/{user}', 'API\UserController@update');
    Route::delete('users/{user}', 'API\UserController@delete');
    Route::put('logout/{id}', 'API\UserController@logout');

    Route::get('/user', function(Request $request){
        return response()->json($request->user(), 200);
    });
    Route::put('users/{id}/{active_status}', 'API\UserController@toggle');

    Route::apiResource('roles', 'API\RolesController');
    Route::apiResource('permissions', 'API\PermissionController');
    Route::get('roles/{type}', 'API\RolesController@rolebytype');
    // Route::resource('users', 'API\UserController');
    
});


